package com.htrucci.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HelloController {

	@RequestMapping("/hello")
	public String index(Model model){
		model.addAttribute("name", "SpringBoot From Htrucci");
		return "hello";
	}
	
	@RequestMapping("/hellovelocity")
	public String indexvelocity(Model model){
		model.addAttribute("name", "SpringBoot From Htrucci");
		return "hellovelocity";
	}	
}
