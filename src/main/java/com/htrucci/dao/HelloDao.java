package com.htrucci.dao;

import org.springframework.data.jpa.repository.JpaRepository;

import com.htrucci.vo.Hello;

public interface HelloDao extends JpaRepository <Hello, Integer> {

	
}
